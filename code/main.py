from d_alg import d_algorithm
from parser import Parser
from circuit_rep import Circuit
from simulator import Simulator
from random_class import random_class
import local_fault_colapsing
import sys


def init_rand(circuit):
    return random_class(circuit)

def init_d_alg(circuit):
    return d_algorithm(circuit)

init_type = {
    'rand': init_rand,
    'd_alg': init_d_alg
}

##############################################################################
# Program Usage:
# python3 main.y [gen_type [filename [max_test_limit [-v]]]]
#       gen_type: 'rand' or 'd_alg'
#       filename: './path/to/input_file'
#       max_test_limit: default is 1000 if unspecified
#       '-v' : stands for verbose
#           **non-verbose output prints number of tests generated vs number of 
#             faults detected and is great for graphing results.
#           **verbose output is much more human friendly and print interesting
#             information as results are computed.
#
#   typical usage examples:
#       python3 main.py d_alg ./input_circuits/s128/c3/circuit.txt
#       python3 main.py rand ./input_circuits/s128/c3/circuit.txt 1500
##############################################################################

# main body of the program
def main():

    # accept filename command line argument
    argv = sys.argv

    # default parameters
    file_name = None
    gen_type = 'rand'
    verbose = False
    max_tests = 1000

    # check for arguments
    if(len(argv) > 1):
        gen_type = argv[1]

    if(len(argv) > 2):
       file_name = argv[2] 

    if(len(argv) > 3):
        max_tests = int(argv[3])
        assert max_tests > 0,"Max number of tests must be greater than 0"

    if(len(argv) > 4):
        verbose = (argv[4] == '-v')

    # parse input file
    # defaults to './input_gates.txt' if file_name == None
    input_dict = Parser().parse(file_name)

    # generate fault list
    fault_list = local_fault_colapsing.local_faults(input_dict)

    # sanity checks
    assert fault_list != None,"Fatal: Could not construct collapsed fault list."
    assert gen_type in init_type,"Fatal: Invalid test generation parameter"

    # print out collapsed fault list

    if(verbose):
        print("Collpased Fault List (%d):" % (len(fault_list)))

        for key,lst in fault_list.items():
            print('wire %s: ' % (key), end='')
            for f in lst:
                print('/%d,' % (f), end='')
            print()
        
        print()

    # Initializations

    # construct circuit object
    circuit = Circuit()
    circuit.read_input_dict(input_dict)

    # construct simulator object
    simulator = Simulator(circuit)

    # construct appropriate generator object
    generator = init_type[gen_type](circuit)

    test_set = []
    num_detected = 0
    test_count = 0

    # main body
    while(len(fault_list) > 0 and test_count < max_tests):

        fault = (None,[])

        # pick non-empty fault from list
        while(len(fault[1]) == 0):
            fault = fault_list.popitem()

        # reinsert fault into list
        fault_list.update({fault[0]: fault[1]})
        
        # generate test
        if(verbose):
            print('generating test for %s/%d' % (fault[0],fault[1][0]))

        test = generator.find_test_for(fault[0],fault[1][0])
        
        # check for successful generation
        assert(test != None), \
        "Failed to generate test for %s/%d" %(fault[0],fault[1][0])

        if(verbose):
            print('test: %s' % (str(test)))

        # add to test set if unique, arbitrary value holder
        if(not(test in test_set)):
            test_set.append(test)
        
        # simulate test vector
        simulator.simulate_inputs(test)

        # find all faults detected, critical path tracing
        faults_detected = simulator.get_faults_detected()

        # check for detected faults in fault_list and remove
        for detected,value in faults_detected.items():
            if(detected in fault_list):
                if(value in fault_list[detected]):

                    # remove found fault from list
                    index = fault_list[detected].index(value)
                    del fault_list[detected][index]

                    # increment counter and housekeeping
                    num_detected = num_detected + 1
                    if (len(fault_list[detected]) == 0):
                        del fault_list[detected]
        
        test_count = test_count + 1
        if(not(verbose)):
            print('%d,%d' % (test_count,num_detected))

    if(verbose):
        print("\nFound %d Tests detecting %d faults:" %(len(test_set),num_detected))
        for test in test_set:
            print(test)

    return 0


if( __name__ == '__main__'):
    sys.exit(main())

