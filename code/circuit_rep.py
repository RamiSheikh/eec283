
class Wire:
    '''A wire should have a unique name to fit into the circuit.
       It then has a single source and multiple output connections.'''
    
    # only used for building circuits, doesn't describe a wire persay...
    types = {'input': 0, 'output': 1}
    type_num = {0: 'input', 1: 'output'}
    is_wire = True
    is_gate = False

    def __init__(self,name,value=2):
        self.name = name
        self.source  = None
        self.outputs = []
        self.value = value

    def set_source_gate(self,source):
        self.source = source

    def add_output(self,gate):
        self.outputs.append(gate)


class Gate:
    '''Gates can be one of several types of logic gates. They can have 
       multiple inputs and a single output. '''

    types = {'~': 0, '&': 1, '|': 2, '^': 3} 
    type_num = {0: '~', 1: '&', 2: '|', 3: '^'}
    is_wire = False
    is_gate = True

    def __init__(self,gate_type):
        self.operator = gate_type
        self.inputs = []
        self.output = None
   
    def add_input_wire(self,input_wire):
        self.inputs.append(input_wire)

    def set_output_wire(self,output):
        self.output = output

    def print_gate(self):
        if not(self.output == None):

            print('%s=' % (self.output.name), end='')

            if(len(self.inputs) == 1):
                print('%s%s' % (Gate.type_num[self.operator],self.inputs[0].name))

            elif (len(self.inputs) > 1):
                print('%s' % (self.inputs[0].name), end='')
                for x in range(1,len(self.inputs)):
                    print('%s%s' % (Gate.type_num[self.operator],self.inputs[x].name),end='')
                print()

            else:
                print()


class Circuit:
    ''' A circuit is a collection of data structures that represent a circuit
        with the goal of easy accessibiltiy for a given set of operations '''

    def __init__(self):
        self.wire_dict = {}
        self.input_dict = {}
        self.output_dict = {}

    def print_ckt(self):
        printed_gates = {}
        print("Circuit:")
        for name,wire in self.wire_dict.items():
            for gate in wire.outputs:
               
                # for all gates not printed
                if not(gate in printed_gates):
                    # add to set
                    printed_gates[gate] = ''
                    # and print
                    gate.print_gate()

    def print_values(self):
        print("Wires:")
        for name,wire in self.wire_dict.items():
            print("%s:\t%d" %(name,wire.value))

    def read_input_dict(self,input_dict):

        # feed gates into circuit
        for out,tup in input_dict.items():

            # (gate, [in0,in1], output)
            # not gate is missing tup[2]
            if(tup[2] != None):
                self.add_gate(tup[0], [tup[1],tup[2]], out)
            else:
                self.add_gate(tup[0], [tup[1]], out)


    def undo_changes(self,decisions,implications):
        
        # iterate through decisions and undo (set to valX === 2)
        for wire in decisions:
            self.wire_dict[wire].value = 2

        for wire in implications:
            self.wire_dict[wire].value = 2

    def apply_changes(self,decisions,implications):
        
        for wire,val in decisions.items():
            self.wire_dict[wire].value = val;

        for wire,val in implications.items():
            self.wire_dict[wire].value = val;


    def add_gate(self,gate_type,inputs,output):
        ''' Used for building circuits. Inputs should be strings, this makes 
            it easier to construct from a text file 
            
            INPUTS: 
                gate_type in {'&','|','^','~'}
                inputs = [string_name0,string_name1,...]
                output = string
        '''
        
        # create gate
        g = Gate(Gate.types[gate_type])

        # add inputs
        for wire in inputs:
            w = self.add_wire(wire, Wire.types['input'])
            
            # connect input and gate
            w.add_output(g)
            g.add_input_wire(w)
    
        # add output
        o = self.add_wire(output, Wire.types['output'])

        # connect output and gate
        o.set_source_gate(g)
        g.set_output_wire(o)

    def add_wire(self, wire, wire_type):
        ''' Prviate function. Fetches/Creates wire and updates dictionaries
            appropriately '''
        w = None
            
        # if wire doesn't exist yet
        if not(wire in self.wire_dict):

            # create wire and file appropriately
            w = Wire(wire)
            self.wire_dict[wire] = w;

            if(wire_type == Wire.types['input']):
                self.input_dict[wire] = w;
            elif(wire_type == Wire.types['output']):
                self.output_dict[wire] = w;
         
        # wire exists
        else:
            w = self.wire_dict[wire]

            # remove from inappropriate dictionary
            if ((wire_type == Wire.types['input']) and (wire in self.output_dict)): 
                self.output_dict.pop(wire)
            if ((wire_type == Wire.types['output']) and (wire in self.input_dict)): 
                self.input_dict.pop(wire)

        return w



