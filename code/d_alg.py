from circuit_rep import Wire
from circuit_rep import Gate
from circuit_rep import Circuit
from tree import Tree
from tree import Node
from copy import deepcopy

def bit_flip(val):
    if (val == 1):
        return 0
    elif(val ==0):
        return 1
    else:
        return 2

class d_algorithm:

    phase_num = {'init': 0, 'propagate': 1, 'justify': 2}
    phase_name = {0: 'init', 1:'propagate', 2:'justify'}

    ##########################################################################
    # PUBLIC FUNCTIONS 
    ##########################################################################

    def __init__(self,base_circuit):
        self.base_circuit = base_circuit
        self.faulty_circuit = None
        self.search_tree = None
        self.fault = None;

        # gate operation lookup tables
        self.prop_functions = {
            Gate.types['&']: self.prop_and,
            Gate.types['|']: self.prop_or,
            Gate.types['^']: self.prop_xor,
            Gate.types['~']: self.prop_not
        }

        self.jst_functions = {
            Gate.types['&']: self.jst_and,
            Gate.types['|']: self.jst_or,
            Gate.types['^']: self.jst_xor,
            Gate.types['~']: self.jst_not
        }


    def set_base_circuit(self,base_circuit):
        self.base_circuit = base_circuit

    def find_test_for(self,wire_name,stuck_at):
        ''' Returns test vector on success or None on failure '''

        assert self.base_circuit != None,"d_algorithm: No circuit specified!"

        # allocate new DFS search tree
        self.search_tree = Tree()
        self.search_tree.location.phase = d_algorithm.phase_num['init']

        # make a copy of base circuit to work on
        self.faulty_circuit = deepcopy(self.base_circuit)

        # set fault
        self.fault = (wire_name,stuck_at);

        # initialize faulty circuit and root Node
        self.init_fault()

        output_vector = None

        while(1):
            output_vector = self.process_node()
            if(output_vector == None):
                self.search_tree.move_to_next_leaf(self.faulty_circuit)
                if(self.search_tree.location.is_root):
                    break
            else:
                break

        return output_vector



    ##########################################################################
    # INTERNAL FUNCTIONS 
    # Top Level Functions:
    ##########################################################################

    def init_fault(self):

        wire_name = self.fault[0]
        stuck_at = self.fault[1]
        circuit = self.faulty_circuit

        # lookup wire
        wire_obj = circuit.wire_dict[wire_name] 
    
        # set wire value to activate
        wire_obj.value = bit_flip(stuck_at)
    
        # create new wire to model fault
        # connect wire-wire : orig-faulty
        faulty_name = '_f_' + wire_name
        fault_wire = Wire(faulty_name,stuck_at)
        fault_wire.source = wire_obj
        fault_wire.outputs.extend(wire_obj.outputs)
        wire_obj.outputs = [fault_wire]

        # if outputs to gate correct gate input
        if(len(fault_wire.outputs) > 0 and fault_wire.outputs[0].is_gate):
            i = fault_wire.outputs[0].inputs.index(wire_obj)
            fault_wire.outputs[0].inputs[i] = fault_wire

        # insert new wire into circuit
        circuit.wire_dict[faulty_name] = fault_wire

        # if wire is an output make new stuck-at wire the output
        if(wire_name in circuit.output_dict):
            circuit.output_dict[faulty_name] = fault_wire
            del circuit.output_dict[wire_name]

        # update decisions in search tree
        self.search_tree.location.decisions[wire_name] = wire_obj.value
        self.search_tree.location.decisions[faulty_name] = stuck_at

        # initialize phase and status information
        self.search_tree.location.phase = d_algorithm.phase_num['propagate']
        self.search_tree.location.prop_frontier = '_f_' + self.fault[0]
        self.search_tree.location.j_frontier = []

        # add faulty wire to justification set if not an input
        if (not(wire_name in self.faulty_circuit.input_dict)):
            self.search_tree.location.j_frontier.append(self.fault[0])

    def process_node(self):

        assert self.search_tree.location.phase != d_algorithm.phase_num['init'], \
        'd_algorithm: process_node() shouldn\'t receive node in init phase'

        if(self.search_tree.location.phase == d_algorithm.phase_num['propagate']):
            valid = self.propagate_fault()
            if(not(valid)):
                return None

        if(self.search_tree.location.phase == d_algorithm.phase_num['justify']):
            valid = self.justify()
            if(not(valid)):
                return None

        output_vector = {}

        for name,wire in self.faulty_circuit.input_dict.items():
            output_vector[name] = wire.value

        return output_vector



    ########################################################################## 
    # Propogation Functions:
    ########################################################################## 

    def propagate_fault(self):
        ''' fault_name is the name of the wire that defines the fault frontier 
            if not specified defaults to the faulty wire '''

        fault_name = self.search_tree.location.prop_frontier

        circuit = self.faulty_circuit
        fault_wire = self.faulty_circuit.wire_dict[fault_name]
        
        # value already initialized
        current_wire = fault_wire

        # iterate until output
        while(not(current_wire.name in circuit.output_dict)):

            # retrieve output gate
            tup = self.prop_follow_wire(current_wire)

            # check for error
            if (tup == None):
                return False

            wire = tup[0]
            gate = tup[1]
            
            self.search_tree.location.prop_frontier = wire.name

            if(gate == None):
                break
            
            # handles updating prop & j frontiers
            current_wire = self.prop_functions[gate.operator](wire,gate)

            if(current_wire == None):
                return False
            
            # redundant but more clear
            self.search_tree.location.prop_frontier = current_wire.name


        self.search_tree.location.phase = d_algorithm.phase_num['justify']
        return True

   
    
    ##########################################################################
    # Justification Functions:
    ##########################################################################

    def justify(self):
        ''' attempts to justify all decisions made '''

        while (len(self.search_tree.location.j_frontier) > 0):
           wire_name = self.search_tree.location.j_frontier.pop() 

           valid = self.back_prop(wire_name)
           if(not(valid)):
               return False

        return True
 
    def back_prop(self,wire_name):
        ''' Justifies a single wire back one gate, core part of the overall 
            justification processs 
            Then creates the appropriate possibilities for justifying inputs
            and adds new justification frontiers to frontier list '''

        # lookup wire object
        wire = self.faulty_circuit.wire_dict[wire_name]

        # propagate value upstream to earlier gate or input
        tup = self.prop_follow_wire(wire,direction=1)

        # collision while propagating upstream
        if (tup == None):
            return False
        
        # successful propagation
        wire = tup[0]
        gate = tup[1]

        # hit an input
        if(gate == None):
            return True
        
        # handle multiple possibilities that arise at gates
        valid = self.jst_functions[gate.operator](wire,gate)

        if(not(valid)):
            return False

        return True


    ##########################################################################
    # Shared Functions:
    ##########################################################################

    def prop_follow_wire(self,current_wire,direction=0):

        while(1):
            assert len(current_wire.outputs) < 2, \
            "d_algorithm: wire %s contains stems" % (current_wire.name)

            if((direction <= 0 and len(current_wire.outputs) == 1) or \
            (   direction >  0 and current_wire.source != None)) :

                if(direction > 0):
                    out = current_wire.source
                else:
                    out = current_wire.outputs[0]

                if(out.is_gate):
                    return (current_wire,out)
                else:
                    valid = self.add_implication(out.name,current_wire.value)

                    if(not(valid)):
                        return None

                    current_wire = out

            else:
                return (current_wire,None)
 
    ##########################################################################
    # Auxiliary Functions:
    ##########################################################################

    def add_decision_to(self,node,wire_name,value):

        wire = self.faulty_circuit.wire_dict[wire_name]
        
        if (wire.value != 2 and wire.value != value):
            return False

        if (wire.value != value):
            node.decisions[wire_name] = value

        return True

    def add_implication_to(self,node,wire_name,value):
        wire = self.faulty_circuit.wire_dict[wire_name]

        if (wire.value != 2 and wire.value != value):
            return False

        if (wire.value != value):
            node.implications[wire_name] = value

        return True


    def add_implication(self,wire_name,value):
        
        wire = self.faulty_circuit.wire_dict[wire_name]

        if (wire.value != 2 and wire.value != value):
            return False

        if (wire.value != value):
            self.search_tree.location.implications[wire_name] = value

        wire.value = value

        return True

    def create_child_node(self):
        node = Node()

        node.phase = deepcopy(self.search_tree.location.phase)
        node.prop_frontier = deepcopy(self.search_tree.location.prop_frontier)
        node.j_frontier = deepcopy(self.search_tree.location.j_frontier)

        return node

    def build_options(self,options):
        ''' options should be a list of dictionaries mapping names to 
        to 4-tuples of (value, decision, j_frontier, p_frontier) where frontier is a boolean 
        indicating if the assignment should be added to the frontier list
        of the new node and dec is True if it should be added to decisions
        list or False if its an implication '''

        final_list = []

        for assignment in options:
            node = self.create_child_node()

            valid = True

            for name,tup in assignment.items():
                val = tup[0]
                decision = tup[1]
                j_frontier = tup[2]
                p_frontier = tup[3]

                if(decision):
                    valid = valid and self.add_decision_to(node,name,val) 
                else:
                    valid = valid and self.add_implication_to(node,name,val)

                if(j_frontier and not(name in self.faulty_circuit.input_dict)):
                    node.j_frontier.append(name)

                if(p_frontier):
                    node.prop_frontier = name

            if(valid):
                final_list.append(node)

        if(len(final_list) == 0):
            return None
        else:
            return final_list



    ##########################################################################
    # Justification functions for handling gates:
    ##########################################################################

    def jst_and(self,wire,gate):
        assert len(gate.inputs) == 2,"d_algorithm: AND gate must have 2 inputs for justification."

        in0 = gate.inputs[0]
        in1 = gate.inputs[1]

        n_list = None

        if(wire.value == 1):
            # both inputs must be one, justify
            n_list = self.build_options([
                {
                    # (val, decision, j_front, p_front)
                    in0.name: (1,1,1,0),
                    in1.name: (1,1,1,0)
                }
            ])
        else:
            # either input needs to be zero
            # only need to justify one, other is Dont Care
            n_list = self.build_options([
                {
                    in0.name: (0,1,1,0)
                },
                {
                    in1.name: (0,1,1,0)
                }
            ])


        if(n_list != None):
            self.search_tree.add_children_and_move(self.faulty_circuit,n_list)
            return True

        else:
            return False


    def jst_or(self,wire,gate):
        assert len(gate.inputs) == 2,"d_algorithm: OR gate must have 2 inputs for justification."

        in0 = gate.inputs[0]
        in1 = gate.inputs[1]

        n_list = None

        if(wire.value == 1):
            # either input needs to be zero
            # only need to justify one, other is Dont Care
            n_list = self.build_options([
                {
                    # (val, decision, j_front, p_front)
                    in0.name: (1,1,1,0)
                },
                {
                    in1.name: (1,1,1,0)
                }
            ])
        else:
            # both inputs must be zero, justify
            n_list = self.build_options([
                {
                    # (val, decision, j_front, p_front)
                    in0.name: (0,1,1,0),
                    in1.name: (0,1,1,0)
                }
            ])

        if(n_list != None):
            self.search_tree.add_children_and_move(self.faulty_circuit,n_list)
            return True

        else:
            return False


    def jst_xor(self,wire,gate):
        assert len(gate.inputs) == 2,"d_algorithm: XOR gate must have 2 inputs for justification."

        in0 = gate.inputs[0]
        in1 = gate.inputs[1]

        n_list = None

        if(wire.value == 1):
            n_list = self.build_options([
                {
                    # (val, decision, j_front, p_front)
                    in0.name: (1,1,1,0),
                    in1.name: (0,1,1,0)
                },
                {
                    in0.name: (0,1,1,0),
                    in1.name: (1,1,1,0)
                }
            ])
        else:
            n_list = self.build_options([
                {
                    # (val, decision, j_front, p_front)
                    in0.name: (0,1,1,0),
                    in1.name: (0,1,1,0)
                },
                {
                    in0.name: (1,1,1,0),
                    in1.name: (1,1,1,0)
                }
            ])

        if(n_list != None):
            self.search_tree.add_children_and_move(self.faulty_circuit,n_list)
            return True

        else:
            return False

    def jst_not(self,wire,gate):
        assert len(gate.inputs) == 1,"d_algorithm: NOT gate must have 1 input for justification."

        in0 = gate.inputs[0]

        valid = self.add_implication(in0, bit_flip(wire.value))

        if(not(valid)):
            return False

        else:
            if(not(in0.name in self.faulty_circuit.input_dict)):
                node.j_frontier.append(name)

            return True



    ##########################################################################
    # Propagation functions for handling gates:
    ##########################################################################
 
    def prop_and(self,wire,gate):

        assert (len(gate.inputs) == 2),"d_agorithm: Invalid number of inputs"

        if(gate.inputs[0].name == wire.name):
            other_wire = gate.inputs[1]
        else:
            other_wire = gate.inputs[0]

        valid = self.add_implication(other_wire.name,1)

        if(not(valid)):
            return None

        if (not(other_wire.name in self.faulty_circuit.input_dict)):
            self.search_tree.location.j_frontier.append(other_wire.name)

        valid = self.add_implication(gate.output.name,wire.value)

        if(not(valid)):
            return None
        else:
            self.search_tree.location.prop_frontier = gate.output.name

        return gate.output

    def prop_or(self,wire,gate):

        assert (len(gate.inputs) == 2),"d_agorithm: Invalid number of inputs"

        if(gate.inputs[0].name == wire.name):
            other_wire = gate.inputs[1]
        else:
            other_wire = gate.inputs[0]

        valid = self.add_implication(other_wire.name,0)

        if(not(valid)):
            return None

        if (not(other_wire.name in self.faulty_circuit.input_dict)):
            self.search_tree.location.j_frontier.append(other_wire.name)

        valid = self.add_implication(gate.output.name,wire.value)

        if(not(valid)):
            return None
        else:
            self.search_tree.location.prop_frontier = gate.output.name

        return gate.output


    def prop_xor(self,wire,gate):
        assert len(gate.inputs) == 2,"d_algorithm: can only handle 2 input XORs"

        if(gate.inputs[0] != wire):
            other_input = gate.inputs[0]
        else:
            other_input = gate.inputs[1]

        
        n_list = self.build_options([ \
            {
                # (val, decision, j_front, p_front)
                other_input.name: (0,1,1,0),
                gate.output.name: (wire.value,0,0,1)
            },
            {
                other_input.name: (1,1,1,0),
                gate.output.name: (bit_flip(wire.value),0,0,1)
            }
        ])

        if(n_list != None):
            self.search_tree.add_children_and_move(self.faulty_circuit,n_list)
            return gate.output

        else:
            return None


    def prop_not(self,wire,gate):
        valid = self.add_implication(gate.output.value, bit_flip(wire.value))

        if (not(valid)):
            return None
        else:
            self.search_tree.location.prop_frontier = gate.output.name

        return gate.output


