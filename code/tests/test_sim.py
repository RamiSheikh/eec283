from d_alg import d_algorithm
from read_txt_file import read_txt_file
from circuit_rep import Wire
from circuit_rep import Gate
from circuit_rep import Circuit
from tree import Tree
from tree import Node
from simulator import Simulator

def print_vector(d):
    for name,val in d.items():
        print("%s : %d" % (name,val))
    print()


# create circuit
ckt_dict = read_txt_file()
ckt = Circuit()
ckt.read_input_dict(ckt_dict)

#ckt.wire_dict['c'].value = 1

# instantiate d_alg object
d = d_algorithm(ckt)

# set fault and prop, note: impl. is incomplete

# fun test, force c = 1 and see how it handles, works well :)
#ckt.wire_dict['c'].value = 1

output = d.find_test_for('a',0)

if (output == None):
    print('find_test returned None. Prop failed.')
    exit(1)

print("Found test for a/0:")
print_vector(output)

print("\nSimulating test vector:")
# instantiate and simulate
sim = Simulator(ckt)
sim.simulate_inputs(output)

# fetch wire values after simulation
values = sim.get_wire_values()
print_vector(values)

# fetch detected faults using critical path tracing
faults = sim.get_faults_detected()
print("\nFaults detected by test")
print_vector(faults)
