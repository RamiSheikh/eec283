from d_alg import d_algorithm
from parser import Parser
from circuit_rep import Wire
from circuit_rep import Gate
from circuit_rep import Circuit
from tree import Tree
from tree import Node

def print_vector(d):
    for name,val in d.items():
        print("%s : %d" % (name,val))
    print()


# create circuit
ckt_dict = Parser().parse()
ckt = Circuit()
ckt.read_input_dict(ckt_dict)

#ckt.wire_dict['c'].value = 1

# instantiate d_alg object
d = d_algorithm(ckt)

# set fault and prop, note: impl. is incomplete

# fun test, force c = 1 and see how it handles, works well :)
#ckt.wire_dict['c'].value = 1

output = d.find_test_for('d',0)

if (output != None):
    print_vector(output)
else:
    print('find_test returned None. Prop failed.')
