from read_txt_file import read_txt_file
from circuit_rep import Wire
from circuit_rep import Gate
from circuit_rep import Circuit

''' This script uses maddie's read_txt_file() code to generate a dictionary 
    representing the input circuit. This is then fed into the Circuit class 
    to build a circuit. The circuit is then printed back out to ensure that
    the representation is accurate. '''

# read file and compile gate dictionary
ckt_dict = read_txt_file()

# instantiate an empty circuit
ckt = Circuit()

# feed gates into circuit
for key,val in ckt_dict.items():
    ckt.add_gate(val[1], [val[0],val[2]], key)

# print out internal representation
ckt.print_ckt()

