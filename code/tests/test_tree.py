from read_txt_file import read_txt_file
from circuit_rep import Wire
from circuit_rep import Gate
from circuit_rep import Circuit
from tree import Tree
from tree import Node

##############################################################################
# Helper functions
##############################################################################
def print_dict(d):
    for name,val in d.items():
        print("%s : %d\t" % (name,val), end='')
    print()


def test_node_ops(ckt):
    print("Testing Node Ops:")
    n0 = Node(decisions={'a': 1, 'b': 0},implications={'m':0})
    print("New Node:")
    print("Decisions:")
    print(n0.decisions)
    print("Implications")
    print(n0.implications)

    ckt.print_values()

    print("Apply")
    n0.apply(ckt)
    ckt.print_values()

    print("Undo")
    n0.undo(ckt)
    ckt.print_values()

    

##############################################################################
# Start tests
##############################################################################

ckt_dict = read_txt_file()

# instantiate an empty circuit
ckt = Circuit()

ckt.read_input_dict(ckt_dict)

tree = Tree()

#test_node_ops(ckt)

n0 = Node(decisions={'a': 1, 'b': 0},implications={'m':0})
n1 = Node(decisions={'a': 0, 'b': 0},implications={'m':0}) 
my_list = [n0,n1]

# not handled well 
#list3 = [ Node(decisions={'n':0},parent=n1),Node(decisions={'n':1},parent=n1)]
#n1.children.extend(list3)

# print out internal representation
ckt.print_ckt()
print("Root:")
tree.location.print()


print("Adding children")
for node in my_list:
    node.print()

print("call add_children_and_move")
tree.add_children_and_move(ckt, my_list)

print("Current Node:")
tree.location.print()

ckt.print_values()


print("Adding Children")
list2 = [Node(decisions={'c':0}), Node(decisions={'c':1})]
for node in list2:
    node.print()

print("call add_children_and_move")
tree.add_children_and_move(ckt, list2)

print("Current Node:")
tree.location.print()

ckt.print_values()

next = tree.location
while(not(next.is_root)):
    print("Calling move_to_next_leaf")
    next = tree.move_to_next_leaf(ckt)
    
    print("Current Node:")
    tree.location.print()
    
    ckt.print_values()
