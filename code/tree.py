from copy import deepcopy


class Node:
    ''' Represents the decisions taken and the implications of that decision. '''
    
    def __init__(self,implications=None,decisions=None,parent=None,is_root=False):

        if (implications == None):
            implications = {}
        if (decisions == None):
            decisions = {}

        self.decisions = decisions
        self.implications = implications
        self.parent  = parent
        self.children = []
        self.is_root = is_root

    def undo(self,circuit):
        ''' Undoes the decsisions and implications represented by a node. '''
        circuit.undo_changes(self.decisions,self.implications)

    def apply(self,circuit):
        ''' Applies the decisions and implications represented by a node'''
        circuit.apply_changes(self.decisions,self.implications)

    def print(self):
        print(self.decisions)
        print(self.children)

class Tree:
    ''' A decisions tree class for handling depth-first search and 
        backracking '''

    ##########################################################################
    # Constructor
    ##########################################################################

    def __init__(self):
        ''' Instantiate an empty decision tree '''
        self.root = Node(is_root=True)
        self.location = self.root

    ##########################################################################
    # Public Functions
    ##########################################################################

    def location_copy():
        return deep_copy(self.location)

    def add_children_and_move(self,circuit,new_children):
        ''' Adds children to current Node and traverses down to first 
            child leaf '''

        # set parent parameter in children
        for child in new_children:
            child.parent = self.location

        # add children to current location
        self.location.children.extend(new_children)

        # traverse down to first leaf, children might have children
        self.drill_down(circuit);

    def find_next_leaf(self):
        ''' Returns the next leaf Node or None if it doesn't exist '''

        current = self.location
        
        while(not(current.is_root)):    
            sibling = get_next_sibling()
            
            if(sibling):
                while(len(sibling.children > 0)):
                    sibling = sibling.children[0];
                return sibling
            else:
                current = current.parent

        return None

    def move_to_next_leaf(self,circuit):
        ''' traverse tree applying and undoing operations on circuit
            until the next leaf is found. Returns None if there is no 
            next leaf else returns new Node '''
        
        while(not(self.location.is_root)):
            sibling = self.get_next_sibling()
            
            self.location.undo(circuit)               # undo node's assignments
            self.location.parent.children.pop(0)      # remove from parent list
            
            if(sibling):
                self.location = sibling
                self.location.apply(circuit)
                self.drill_down(circuit)            # DFS down to leaf
                return self.location                  # return new location
            else:
                self.location = self.location.parent

        return self.location

    ##########################################################################
    # Internal helper functions
    ##########################################################################

    def drill_down(self,circuit):
        ''' Private Method. Traverses down a decision path '''
        while(len(self.location.children) > 0):
            self.location = self.location.children[0]
            self.location.apply(circuit)


    def get_next_sibling(self):
        ''' Private method. Returns the next sibling or None if doesn't exist '''
        if(len(self.location.parent.children) > 1):
            return self.location.parent.children[1]
        else:
            return None
