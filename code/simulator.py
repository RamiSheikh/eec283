from circuit_rep import Wire
from circuit_rep import Gate
from circuit_rep import Circuit
from tree import Tree
from tree import Node
from copy import deepcopy

class Simulator:

    and_sim = {
        (0,0): 0,
        (0,1): 0,
        (1,0): 0,
        (1,1): 1,
        (0,2): 0,
        (2,0): 0,
        (2,2): 2,
        (1,2): 2,
        (2,1): 2
    }
    or_sim = {
        (0,0): 0,
        (0,1): 1,
        (1,0): 1,
        (1,1): 1,
        (0,2): 2,
        (2,0): 2,
        (2,2): 2,
        (1,2): 1,
        (2,1): 1
    }
    xor_sim = {
        (0,0): 0,
        (0,1): 1,
        (1,0): 1,
        (1,1): 0,
        (0,2): 2,
        (2,0): 2,
        (2,2): 2,
        (1,2): 2,
        (2,1): 2
    }
    not_sim = {
        0: 1,
        1: 0,
        2: 2
    }


    ##########################################################################
    # Public Functions
    ##########################################################################

    def __init__(self,circuit):
        assert circuit != None,"simulator: Must be instantiaited with a valid circuit"

        self.circuit = circuit
        self.sim = None
        self.prop_functions = {
            Gate.types['&']: self.prop_and,
            Gate.types['|']: self.prop_or,
            Gate.types['^']: self.prop_xor,
            Gate.types['~']: self.prop_not
        }

        self.crit_functions = {
            Gate.types['&']: self.crit_and,
            Gate.types['|']: self.crit_or,
            Gate.types['^']: self.crit_xor,
            Gate.types['~']: self.crit_not
        }
        
    def set_circuit(self,circuit):
        ''' If you need to switch out the circuit being simulated '''

        assert circuit != None,"simulator: Must be instantiaited with a valid circuit"
        self.circuit = circuit
        self.sim = None

    def simulate_inputs(self,input_values):
        ''' Pass in input values and simulate circuit, works with X's 
            uses agreed upon standard, X = 2 '''

        self.sim = deepcopy(self.circuit)
        wire_list = []
        
        for name,value in input_values.items():
            wire = self.sim.wire_dict[name]
            wire.value = value
            wire_list.append(wire)

        while (len(wire_list) > 0):
            wire = wire_list.pop()
            # may append to list if value changes a gate output
            self.propagate_wire(wire,wire_list)


    # Functions to be called after simulating values 

    def get_faults_detected(self):
        ''' After simulation this will use critical path tracing to find 
            all faults detected by the input set '''

        critical_wires = {}
        wire_list = []

        for name,wire in self.sim.output_dict.items():
            wire_list.append(wire)
        
        while(len(wire_list) > 0):
            wire = wire_list.pop()
            self.critical_path_trace(wire,wire_list,critical_wires)

        return critical_wires


    def get_wire_values(self):
        ''' will return a dictionary of all wire names and their values '''
        if(self.sim == None):
            return None

        return self.get_values(self.sim.wire_dict)

    def get_input_values(self):
        ''' will return a dictionary of all the input wires and their values ''' 
        if(self.sim == None):
            return None

        return self.get_values(self.sim.input_dict)

    def get_output_values(self):
        ''' will return a dictionary of all the output wires and their values '''
        if(self.sim == None):
            return None

        return self.get_values(self.sim.output_dict)


    ##########################################################################
    # Internal functions
    ##########################################################################

    def get_values(self,dictionary):
        d0 = {}

        for key,obj in dictionary.items():
            d0[key] = obj.value

        return d0

    def critical_path_trace(self,wire,wire_list,critical_wires):

        while(1):
            if(wire.name in critical_wires):
                return
            else:
                critical_wires[wire.name] = self.not_sim[wire.value]

            if(wire.source != None):
                parent = wire.source

                if(parent.is_gate):
                    self.crit_functions[parent.operator](parent,wire_list)
                    return

                else:
                    wire = parent
            else:
                return

    def propagate_wire(self,wire,wire_list):

        while(1):
            assert len(wire.outputs) < 2, \
            "simulator: wire %s contains stems" % (wire.name)

            if(len(wire.outputs) == 1) :

                out = wire.outputs[0]

                if(out.is_gate):
                    self.prop_functions[out.operator](out,wire_list)
                    return
                else:
                    out.value = wire.value
                    wire = out

            else:
                return

    def crit_and(self,gate,wire_list):
        assert len(gate.inputs) == 2,"simulator: AND gate has invalid number of inputs"

        # (output, in0, in1) => critical input indices
        # assumption, critical value cannot be X
        find_crit = {
            (1,1,1): [0,1],
            (0,0,0): [],
            (0,0,1): [0],
            (0,0,2): [0],
            (0,1,0): [1],
            (0,2,0): [1],
        }

        indices = find_crit[(gate.output.value,gate.inputs[0].value,gate.inputs[1].value)]

        for index in indices:
            wire_list.append(gate.inputs[index])

    def crit_or(self,gate,wire_list):
        assert len(gate.inputs) == 2,"simulator: AND gate has invalid number of inputs"

        # (output, in0, in1) => critical input indices
        # assumption, critical value cannot be X
        find_crit = {
            (1,1,1): [],
            (0,0,0): [0,1],
            (1,0,1): [1],
            (1,2,1): [1],
            (1,1,0): [0],
            (1,1,2): [0]
        }

        indices = find_crit[(gate.output.value,gate.inputs[0].value,gate.inputs[1].value)]

        for index in indices:
            wire_list.append(gate.inputs[index])

    def crit_xor(self,gate,wire_list):
        assert len(gate.inputs) == 2,"simulator: AND gate has invalid number of inputs"
        wire_list.append(gate.inputs[0])
        wire_list.append(gate.inputs[1])

    def crit_not(self,gate,wire_list):
        assert len(gate.inputs) == 1,"simulator: AND gate has invalid number of inputs"
        wire_list.append(gate.inputs[0])


    def prop_and(self,gate,wire_list):
        assert len(gate.inputs) == 2,"simulator: AND gate has invalid number of inputs"
        
        result = self.and_sim[(gate.inputs[0].value,gate.inputs[1].value)]

        if(result != gate.output.value):
            wire_list.append(gate.output)
            gate.output.value = result

    def prop_or(self,gate,wire_list):
        assert len(gate.inputs) == 2,"simulator: OR gate has invalid number of inputs"


        result = self.or_sim[(gate.inputs[0].value,gate.inputs[1].value)]

        if(result != gate.output.value):
            wire_list.append(gate.output)
            gate.output.value = result

    def prop_xor(self,gate,wire_list):
        assert len(gate.inputs) == 2,"simulator: XOR gate has invalid number of inputs"

        result = self.xor_sim[(gate.inputs[0].value,gate.inputs[1].value)]

        if(result != gate.output.value):
            wire_list.append(gate.output)
            gate.output.value = result

    def prop_not(self,gate,wire_list):
        assert len(gate.inputs) == 1,"simulator: NOT gate has invalid number of inputs"

        result = self.not_sim[gate.inputs[0].value]

        if (result != gate.output.value):
            wire_list.append(gate.output)
            gate.output.value = result

