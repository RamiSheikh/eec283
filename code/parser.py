# libraries
import re


class Parser:

    def __init__(self):
        pass
    
    def parse(self,input_gate_file=None):

        if(input_gate_file == None):
            input_gate_file = './input_gates.txt'

        netlist = {}

        # read in the file
        with open(input_gate_file) as f:
            lines = f.readlines()

        # put the lines into a dictionary
        for line in lines:

            # clean up line
            line = re.sub(' ' , '', line)
            line = re.sub('\n', '', line)

            # extract gate and name info
            gate_tup = self.gate_type(line)
            var_names = self.extract_names(gate_tup,line)

            # map: output => (gate, in0, in1)
            netlist[var_names[0]] = (gate_tup[0], var_names[1], var_names[2])

        return netlist

    def extract_names(self,gate_tup,line):

        gate = gate_tup[0]  # gate string
        start = gate_tup[1] # gate index
        end = gate_tup[2]   # index first char after gate

        equal = line.find('=')

        assert equal > 0,"Invalid netlist syntax, line:\n%s\n" % (line) 
        
        name0 = line[0:equal]
        name1 = None
        name2 = None

        if(gate != '~'):
            name1 = line[equal+1:start]            
            name2 = line[end:len(line)]
        else:
            name1 = line[end:len(line)]

        return (name0,name1,name2)


    def gate_type(self,line):
       
        index = line.find('!&')
        if (index != -1):
            return ('!&',index,index+2)

        index = line.find('!|')
        if (index != -1):
            return ('!|',index,index+2)

        index = line.find('!^')
        if (index != -1):
            return ('!^',index,index+2)

        index = line.find('&')
        if (index != -1):
            return ('&',index,index+1)

        index = line.find('|')
        if (index != -1):
            return ('|',index,index+1)

        index = line.find('^')
        if (index != -1):
            return ('^',index,index+1)

        index = line.find('~')
        if (index != -1):
            return ('~',index,index+1)

        return None
