

# Constants
fault0 = 1
no_fault0 = 0
fault1 = 1
no_fault1 = 0

# Funtion: Assign the faults to the wires.  Changes fault_dict
# Input: key_temp: the wire name
#       fault0 = 1 if there is a fault 0.  0 if not
#       fault1 = 1 if there is a fault 0.  0 if not
#
def faults(fault_dict, key_temp, fault0, fault1):

    fault_list = []
    
    if (key_temp in fault_dict):
        fault_list = fault_dict.get(key_temp)

    if (fault0 == 1 and not(0 in fault_list)):
        fault_list.append(0)

    if (fault1 == 1 and not(1 in fault_list)):
        fault_list.append(1)

    fault_dict.update({key_temp:fault_list})


# Find the local faults for wires
#
#       Ouput: ** fault_dict- Dictionary with the key as the wire and the
#                 value as  list of 1, 0, or both.  
#              ** There will never be repeate value
#
def local_faults(netlist_dict=None):

    fault_dict = {}

    if(netlist_dict == None):
        return None

    for out, tup in netlist_dict.items(): # go through dictionary

        gate = tup[0]
        in0  = tup[1]
        in1  = tup[2]
        
        if   (gate == '&'):
            faults(fault_dict, in0, no_fault0, fault1  )
            faults(fault_dict, in1, no_fault0, fault1  )
            faults(fault_dict, out, fault0   , no_fault1)

        elif (gate == '!&'):
            faults(fault_dict, in0, no_fault0, fault1)
            faults(fault_dict, in1, no_fault0, fault1)
            faults(fault_dict, out, no_fault0, fault1)

        elif (gate == '|'):
            faults(fault_dict, in0, fault0   , no_fault1)
            faults(fault_dict, in1, fault0   , no_fault1)
            faults(fault_dict, out, no_fault0, fault1   )
            
        elif (gate == '!|'):
            faults(fault_dict, in0, fault0, no_fault1)
            faults(fault_dict, in1, fault0, no_fault1)
            faults(fault_dict, out, fault0, no_fault1)

        elif (gate == '^'):
            faults(fault_dict, in0, fault0, fault1)
            faults(fault_dict, in1, fault0, fault1)
            faults(fault_dict, out, fault0, fault1)
            
        elif (gatae == '!^'):
            faults(fault_dict, in0, fault0, fault1)
            faults(fault_dict, in1, fault0, fault1)
            faults(fault_dict, out, fault0, fault1)

        elif (gate == '~'):
            faults(fault_dict, out, fault0, fault1)

    return fault_dict

