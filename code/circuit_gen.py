import sys
from random import seed
from random import randint

def wire_list(start,num):
    lst = []
    for i in range(start,start+num):
        lst.append('w' + str(i))
    return lst

def rand_op():
    r = randint(1,75)

    if( r >= 1 and r <= 25):
        return '|'
    elif (r >= 26 and r <= 50):
        return '&'
    elif (r >= 51 and r <= 75):
        return '^'

def gen_stage(input_list,output_list):

    assert((len(input_list)+1)//2 == len(output_list)), \
    "Stage input and output sizes do not agree"

    start_index = 0
    
    # if odd number of inputs, first is a not gate
    if (len(input_list)%2 == 1):
        print('%s=~%s' % (output_list[0],input_list[0]))
        start_index = 1

    # for rest, 2 inputs -> random gate -> 1 output
    for i in range(start_index,len(input_list),2):
        print('%s=%s%s%s' % (output_list[i//2],input_list[i],rand_op(),input_list[i+1]))


def main():

    argv = sys.argv
    inputs = 32
    random_seed = None

    if(len(argv) > 1):
        inputs = int(argv[1]) 

        # require even number of inputs
        assert inputs>1, \
        "Parameters: number of inputs must be greater than 1"

    if(len(argv) > 2):
        random_seed = int(argv[2])

    seed(a=random_seed)

    # generate stages until you have one output
    #
    # 0 | 25    -> 13      | 25 | 13     -> 7       |
    #   | (0,24)   (25,37) |    | (25,37)   (38,44) | ... 

    # loop init
    next_int = 0
    in_list = wire_list(next_int,inputs)

    while(len(in_list) > 1):

        # setup
        outputs = (inputs+1)//2
        out_list = wire_list(next_int+inputs,outputs)


        # code
        gen_stage(in_list,out_list)

        
        # increment
        next_int = next_int + inputs
        in_list = out_list
        inputs = outputs



    return 0

# allows for fetching program return code from shell
if( __name__ == '__main__'):
    sys.exit(main())

