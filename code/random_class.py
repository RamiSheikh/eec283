from copy import deepcopy
import random

class random_class: 
    
    def __init__(self,circuit):
        random.seed()
        self.inputs = deepcopy(circuit.input_dict)

        for key in self.inputs:
            self.inputs[key] = 2


    def find_test_for(self, DC0, DC1):

        for key in self.inputs:
            self.inputs[key] = random.randint(0,1)

        return deepcopy(self.inputs)
 
