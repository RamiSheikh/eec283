clear;
clc;

data = cell(5,11,12);
sizes = [16,32,64,128,256];

for s = 1:5
    sz = sizes(s);
    folder = ['input_ciruits/s' num2str(sz)];
    for ckt = 1:10
        path = [folder, '/c', num2str(ckt),'/'];
        
        % d_alg
        [a,b] = importfile([path, 'd_alg.txt']);
        data{s,ckt,1} = zeros(2,length(a));
        data{s,ckt,1} = [a';b'];
        
        % random
        for r = 1:10
            [a,b] = importfile([path, 'r', num2str(r), '.txt']);
            data{s,ckt,r+2} = zeros(2,length(a));
            data{s,ckt,r+2} = [a';b'];
        end
    end
end

hold off;
fn = 1;

% compute averages of random trials
for sz = 1:5
        
    for ckt = 1:10

        mx = 0;
        idx = 0;
        for i = 1:10
           l = length(data{sz,ckt,i+2});
           mx = max(mx,l);
           if(mx == l)
               idx = i+2;
           end
           hold on;
        end

        data{sz,ckt,2} = zeros(2,mx);
        data{sz,ckt,2}(1,:) = data{sz,ckt,idx}(1,:);    

        f = figure(fn);

        for i = 1:10
            % find length and last value
            len = length(data{sz,ckt,i+2}(2,:));
            last_val = data{sz,ckt,i+2}(2,len);

            % extend to match max length trial, fill end with last val
            full = [data{sz,ckt,i+2}(2,:), (last_val * ones(1,mx-len))];

            % sum data points
            data{sz,ckt,2}(2,:) = data{sz,ckt,2}(2,:) + full;
            plot(full'); hold on;
        end
        data{sz,ckt,2}(2,:) = data{sz,ckt,2}(2,:) ./ 10;

        mysave(f,sz,ckt,'rands');
        close(fn);
        fn = fn + 1;
        hold off;

    end
end

% compute averages over similar sized circuits
for sz = 1:5

    cmx = 0;
    cidx = 0;
    for ckt = 1:10
       l1 = length(data{sz,ckt,1});
       l2 = length(data{sz,ckt,2});
       cmx = max([cmx,l1,l2]);
       
       if(cmx == l1)
           cidx = [ckt,1];
       elseif (cmx == l2)
           cidx = [ckt,2];
       end
    end
    
    data{sz,11,1} = zeros(2,cmx);
    data{sz,11,2} = zeros(2,cmx);
    data{sz,11,1}(1,:) = data{sz,cidx(1),cidx(2)}(1,:);
    data{sz,11,2}(1,:) = data{sz,cidx(1),cidx(2)}(1,:);

    f1 = figure(fn);
    f2 = figure(fn+1);
    
    for ckt = 1:10
        
        % find length and last value
        len1 = size(data{sz,ckt,1}(2,:),2);
        len2 = size(data{sz,ckt,2}(2,:),2);
        
        last_val1 = data{sz,ckt,1}(2,len1);
        last_val2 = data{sz,ckt,2}(2,len2);

        % extend to match max length trial, fill end with last val
        full1 = [data{sz,ckt,1}(2,:), (last_val1 * ones(1,cmx-len1))];
        full1 = full1 ./ last_val1;
        full2 = [data{sz,ckt,2}(2,:), (last_val2 * ones(1,cmx-len2))];
        full2 = full2 ./ last_val1;
        
        % sum data points
        data{sz,11,1}(2,:) = data{sz,11,1}(2,:) + full1;
        data{sz,11,2}(2,:) = data{sz,11,2}(2,:) + full2;
        
        figure(fn);
        plot(full1); hold on;
        figure(fn+1);
        plot(full2); hold on;
        
    end
    
    data{sz,11,2}(2,:) = data{sz,11,2}(2,:) ./ 10;
    data{sz,11,1}(2,:) = data{sz,11,1}(2,:) ./ 10;
    
    mysave(f1,sz,0,'Dalg');
    mysave(f2,sz,0,'randavg');
    
    figure(fn); hold off;
    figure(fn+1); hold off;

    close(f1);
    close(f2);
    fn = fn + 2;

end

% plot d_alg vs random averages at each size
for sz = 1:5
    f = figure(fn);
    hold on;
    
    plot(data{sz,11,1}(1,:),data{sz,11,1}(2,:));
    plot(data{sz,11,2}(1,:),data{sz,11,2}(2,:));
    legend('avg d-alg', 'avg rand','Location','southeast');
    hold off;
    
    mysave(f,sz,0,'D-alg-Vs-Random');
    close(f);
    
    fn = fn + 1;
end


f = figure(1);
close(f);

function mysave(f,sz,ckt,desc)
    if(ckt == 0)
        t = ['sz', num2str(2^(sz+3)), '-', desc];        
    else
        t = ['sz', num2str(2^(sz+3)), '-ckt', num2str(ckt), '-', desc];
    end
    
    name = ['pics/', t, '.png' ];
    figure(f);
    title(t);
    xlabel('# vectors');
    
    if(ckt == 0)
       ylabel('fault coverage');
    else
        ylabel('# detected faults');
    end
    saveas(f,name);
end




