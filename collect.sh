#!/bin/bash

##############################################################################
# This scripts generates 10 random circuits for each circuit size. Generating
# 10 circuits will hopefully allow us to characterize performance across the 
# stochastic behavior of the circuit generation. The script then
# runs the d_algorithm test generation method until all tests are found. The
# random test generation method is ran 10 times with a limit of 2x the number
# of tests that the d_algorithm required. The multiple runs are used to 
# accurately characterize the stochastic behaviour of the random test 
# generation method. 
#
# The output is logged for later analysis with matlab
##############################################################################

list=`echo "16 32 64 128 256"` 
output_folder="input_circuits"

mkdir $output_folder
pushd $output_folder > /dev/null

for size in $list; do

    foldername="s$size"
    mkdir $foldername
    pushd $foldername > /dev/null 

    for ckt_num in `seq 1 10`; do
        ckt_name="c$ckt_num"
        mkdir $ckt_name
        pushd $ckt_name > /dev/null

        # create circuit
        python3 ../../../code/circuit_gen.py $size > circuit.txt

        # run d_alg
        python3 ../../../code/main.py d_alg circuit.txt > d_alg.txt

        # num_tests(rand) = 2 * num_tests(d_alg)
        # allow for the random test method to generate twice as many
        # tests as the d_alg if necessary
        iter=`cat d_alg.txt | tail -1 | sed 's/.*,//'`
        iter=`python3 -c "print(int($iter*2))"`

        # run rand 10 times
        for i in `seq 1 10`; do
            python3 ../../../main.py rand circuit.txt $iter > "r$i".txt
        done
        
        popd > /dev/null
    done
    popd > /dev/null
done
popd > /dev/null 
